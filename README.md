# Deploy kubernetes-dashboard on Rancher 2.x cluster exposed using NodePort

This has been updated to install Dashboard v2.0.0, see below for pre v2.0.0 instructions

## Requirements
- `kubectl` (https://kubernetes.io/docs/tasks/tools/install-kubectl/)

## Step 1: Generate kubeconfig from the UI

Generate the kubeconfig file for your cluster using the `Kubeconfig File` button in the Cluster view of your cluster.
Save the generated file as `$HOME/.kube/config` and run `kubectl get nodes` to verify it works.

## Step 2: Deploy kubernetes dashboard

Deploy the kubernetes dashboard by using the recommended deployment definition.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
kubectl rollout status deploy/kubernetes-dashboard -n kubernetes-dashboard
```

Reference: https://github.com/kubernetes/dashboard/blob/master/README.md

## Step 3: Change Service Type to NodePort

For this guide, we are using a NodePort to access the kubernetes dashboard. By default, the Service gets created as `ClusterIP`.
You can change that by using the following command:

```
kubectl get svc/kubernetes-dashboard  -n kubernetes-dashboard  -o yaml | sed 's/ClusterIP/NodePort/g' | kubectl apply -f -
service "kubernetes-dashboard" configured
```

## Step 4: Create ServiceAccount and token to login

Copy the following YAML content and save it as `dashboard.yml`:

```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
```

Run the following command to create the needed resources:

```
kubectl create -f dashboard.yml
```

Retrieve the token which can be used to login:

```
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

Save the string after `token:` so you can login into the dashboard.

Example output:

```
Name:         admin-user-token-7zzll
Namespace:    kubernetes-dashboard
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: admin-user
              kubernetes.io/service-account.uid: 74d52b6f-da28-47e9-b330-eb4ee72d5b8c

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1017 bytes
namespace:  20 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6InRrN1AzWHBfcFBXY0ZWLTdJS2lEMTJlQXM3UTBERkd1SkhRVW8tSWFMWkEifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTd6emxsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI3NGQ1MmI2Zi1kYTI4LTQ3ZTktYjMzMC1lYjRlZTcyZDViOGMiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.ltaVCHfLogzHhyM8YqaKWn9g-TwfNGXGvJ4Bg3g0z52OjmJr0Nch8JpBtS0EJG-6mQP2oUxSHt1hTuVyPHFPcf65JpJct5Q0kNIDcJhUaAt_f24Rge9rpGrEsmgVAsOUNCqZgdU3b_h5s0BiCVMBB71lCS83F3JSbQ_kUI1SG-RL1np_xhS_gYkiygKMyzIF2v6-k7PN09EAS1m49FpDt-XRraF1YTGQVx6SMIOA_scT-b3Vpgux1mMY5SqjP9ciTVIkNxSy5v7SKUnA8criglBGhBuGGz4zV0xGRFo_DRJRwX5t-oPVY20IbIp0IcoxUe1uCNY1MByntoubahsRug
```

Reference: https://github.com/kubernetes/dashboard/wiki/Creating-sample-user

## Step 5: Login to the dashboard

We need to access the IP of one of the nodes added to your cluster, on the randomly chosen `NodePort` we configured earlier.
Below are two command to automatically find these parameters:

```
NODEPORT=`kubectl get services/kubernetes-dashboard -n kubernetes-dashboard  -o jsonpath="{.spec.ports[0].nodePort}"`
for NODE in `kubectl get no -o jsonpath='{range.items[*].status.addresses[?(@.type=="InternalIP")]}{"https://"}{.address}{"\n"}{end}'`; do echo $NODE:$NODEPORT; done
for NODE in `kubectl get no -o jsonpath='{range.items[*].status.addresses[?(@.type=="Hostname")]}{"https://"}{.address}{"\n"}{end}'`; do echo $NODE:$NODEPORT; done
```

Use one of the printed URLs to visit the kubernetes dashboard in your browser.

When prompted to sign in, choose **Token** and use the token you saved in step 4.


## Extra: kubectl proxy

```
kubectl proxy
```

Access dashboard this URL, replacing CLUSTERID with your clusterid (shown in the address bar of your browser):

http://localhost:8001/k8s/clusters/CLUSTERID/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/


================================================================================================================

The instructions for installing dashboard before v2.0.0 was released can be found below:

## Step 1: Generate kubeconfig from the UI

Generate the kubeconfig file for your cluster using the `Kubeconfig File` button in the Cluster view of your cluster.
Save the generated file as `$HOME/.kube/config` and run `kubectl get nodes` to verify it works.

## Step 2: Deploy kubernetes dashboard

Deploy the kubernetes dashboard by using the recommended deployment definition.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl rollout status deploy/kubernetes-dashboard -n kube-system
```

Reference: https://github.com/kubernetes/dashboard/blob/master/README.md

## Step 3: Change Service Type to NodePort

For this guide, we are using a NodePort to access the kubernetes dashboard. By default, the Service gets created as `ClusterIP`.
You can change that by using the following command:

```
kubectl get svc/kubernetes-dashboard  -n kube-system  -o yaml | sed 's/ClusterIP/NodePort/g' | kubectl apply -f -
service "kubernetes-dashboard" configured
```

## Step 4: Create ServiceAccount and token to login

Copy the following YAML content and save it as `dashboard.yml`:

```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
```

Run the following command to create the needed resources:

```
kubectl create -f dashboard.yml
```

Retrieve the token which can be used to login:

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```
Save the string after `token:` so you can login into the dashboard.

Example output:

```
Name:         admin-user-token-6gl6l
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name=admin-user
              kubernetes.io/service-account.uid=b16afba9-dfec-11e7-bbb9-901b0e532516

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1025 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTZnbDZsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiMTZhZmJhOS1kZmVjLTExZTctYmJiOS05MDFiMGU1MzI1MTYiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.M70CU3lbu3PP4OjhFms8PVL5pQKj-jj4RNSLA4YmQfTXpPUuxqXjiTf094_Rzr0fgN_IVX6gC4fiNUL5ynx9KU-lkPfk0HnX8scxfJNzypL039mpGt0bbe1IXKSIRaq_9VW59Xz-yBUhycYcKPO9RM2Qa1Ax29nqNVko4vLn1_1wPqJ6XSq3GYI8anTzV8Fku4jasUwjrws6Cn6_sPEGmL54sq5R4Z5afUtv-mItTmqZZdxnkRqcJLlg2Y8WbCPogErbsaCDJoABQ7ppaqHetwfM_0yMun6ABOQbIwwl8pspJhpplKwyo700OSpvTT9zlBsu-b35lzXGBRHzv5g_RA
```

Reference: https://github.com/kubernetes/dashboard/wiki/Creating-sample-user

## Step 5: Login to the dashboard

We need to access the IP of one of the nodes added to your cluster, on the randomly chosen `NodePort` we configured earlier.
Below are two command to automatically find these parameters:

```
NODEPORT=`kubectl get services/kubernetes-dashboard -n kube-system  -o jsonpath="{.spec.ports[0].nodePort}"`
for NODE in `kubectl get no -o jsonpath='{range.items[*].status.addresses[?(@.type=="InternalIP")]}{"https://"}{.address}{"\n"}{end}'`; do echo $NODE:$NODEPORT; done
```

Use one of the printed URLs to visit the kubernetes dashboard in your browser.

When prompted to sign in, choose **Token** and use the token you saved in step 4.


## Extra: kubectl proxy

```
kubectl proxy
```

Access dashboard this URL, replacing CLUSTERID with your clusterid (shown in the address bar of your browser):

http://localhost:8001/k8s/clusters/CLUSTERID/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
